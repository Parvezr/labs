## Lab: Creating a Watson Studio Project with Jupyter Notebooks


This tutorial walks you through setting up an account on the IBM Cloud and a project in Watson Studio such that you can use jupyter notebooks for your work.

1. Please use the following link to create an IBM Cloud Account. It's completely for free, you don't need a credit card and the account never expires. 
https://cocl.us/Watson_Studio_edX_DS0105EN
(Note: Please note that if you are using this link, your registration is tracked to this course. This way you are indirectly supporting the course.)

In case you are getting an error like below, please try with an different email address before you contact support. E.g. a non - gmail address.


![image](images/DS0105EN_Lab1_Image1.png)

2. Once you've completed registration and confirmed your email address, please open dataplatform.cloud.ibm.com and click on "Log In"
![image](images/DS0105EN_Lab1_Image2.png)
3. Now please click on "Create a project"
![image](images/DS0105EN_Lab1_Image3.png)
4. Now please click on "Create an empty project"
![image](images/DS0105EN_Lab1_Image4.png)
5. Under "name", please type "default", then please click on "Add" under "Define Storage"
![image](images/DS0105EN_Lab1_Image5.png)
6. Once you see the screen below, please scroll down
![image](images/DS0105EN_Lab1_Image6.png)
7. Please make sure the "Lite" plan is selected, then please click on "Create"
![image](images/DS0105EN_Lab1_Image7.png)
8. Please click on "Confirm"
9. Please click on "Refresh"
![image](images/DS0105EN_Lab1_Image8.png)
10. Please click on "Create" to finalize project creation
![image](images/DS0105EN_Lab1_Image9.png)
Congratulations, this concludes the first part of the tutorial. Please take a moment to follow through the next steps to learn how you can use Watson Studio jupyter notebooks.

1. Please click on "Add to project"
![image](images/DS0105EN_Lab1_Image10.png)
2. Here you can select an abundance of tools, but let's go for jupyter notebooks first. Please click on "Notebook"
![image](images/DS0105EN_Lab1_Image11.png)
3. In order to not use up your monthly free compute credits just select the "Default Python 3.6 Free" runtime
![image](images/DS0105EN_Lab1_Image12.png)
4. Please click on "Create notebook"
![image](Iimages/DS0105EN_Lab1_Image13.png)
5. Just wait until the notebook appears. In case you are interested. The jupyter enterprise gateway has requested resources on the Kubernetes cluster IBM hosts for serving the jupyter kernel backing your notebook
![image](images/DS0105EN_Lab1_Image14.png)
6. Now you're ready to code!
![image](images/DS0105EN_Lab1_Image15.png)
This concludes this tutorial.
